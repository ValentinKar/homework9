<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки


class Product   // товар
{
	public static $living = false;  // статическое свойство
	public $category; // категория
	public $brand;  // марка
	public $color;  // цвет 
	private $price; // цена
	public $discount; // скидка
	public $type;   // 


	public function __construct($category, $brand)  // конструктор устанавливает категорию и марку
	{
		$this ->category = $category; 
		$this->brand = $brand; 
	}

	public function setPrice($price)  // изменяю цену
	{
		$this->price = $price;
	}

	public function getPrice()   // узнаю цену
	{
		if ($this ->discount) {
			return round($this->price - ($this->price * $this ->discount/100) );
		}
		else {
			return $this ->price;
		}
	}
}


class Ballpen   // шариковая ручка
{
	public static $living = false;  // статическое свойство
	private $brand;  // марка шариковой ручки
	private $price;  // цена
	private $type = 'roller';    // гелевая или нет
	private $rod = true;  // есть стержень или нет
	private $ink = 100;  // сколько чернил осталось в стержне
	public $work = true;  // работает или нет - если есть кнопка как у старых авторучек

	public function setBrand($text)  // изменяю марку авторучки
	{
		$this->brand = $text;
	}

	public function getBrand()   // узнаю марку авторучки
	{
		return $this->brand;
	}

		public function setType($text)  // изменяю тип авторучки
		{
			$this->type = $text; 
		} 

		public function getType()   // узнаю тип авторучки
		{
			return $this->type; 
		} 


	public function turnOn()   // включить
	{
		$this->work = true; 
		return $this ->work;
	}

	public function turnOff()   // выключить
	{
		$this->work = false; 
		return $this->work; 
	}

		public function insertRod()    // вставить стержень
		{
			$this ->rod = true; 
			return $this ->rod;
		}

		public function getRod()    // достать стержень
		{
			$this ->rod = false; 
			return $this ->rod;
		}

	public function write($text)    // написать текст
	{ 
			if ($this ->rod === false) {
				exit;
			}

		$i = strlen($text); 
		$this ->turnOn(); 

			if ($this ->type = 'gel') {
			$this ->ink = $this ->ink - $i / 5;
			}

			if ($this ->type = 'roller') {
			$this ->ink = $this ->ink - $i / 10;
			}

		echo "$text"; 
		return ' (чернил осталось: ' . $this ->ink . ' ед.) <br />'; 
	}

}