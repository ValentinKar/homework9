<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once 'Class0.php'; 
require_once 'Class1.php'; 

$Ford = new AutoCar('Ford');   // новая машина марки Ford
$Toyota = new AutoCar('Toyota');  // новая машина марки Toyota
$BMW = new AutoCar('BMW');    // новая машина марки BMW
// $BMW = new AutoCar();  // ошибка
$BMW->color = 'red';        // BMW покрасили красным цветом 
$BMW->setType('passenger');  //  BMW сделали легковым авто
echo $BMW->getType();    // узнали, что BMW - легковая
echo '<br />'; 
echo $BMW ->courseNew(45, 5);    // повернули руль на 45 град. и держим 5 сек. - получили новый курс
echo '<br />'; 
echo $BMW ->speed(5, 3);     // надавили на газ усилием в 5 ед. и получили скорость
echo '<br />'; 
echo AutoCar::$staticCounter;    // определили кол-во новых авто
echo '<br /><br />'; 

$tv_LG = new Televisor();       // новый телевизор LG
$tv_samsung = new Televisor();    // новый телевизор Samsung
$tv_samsung->setBrand('samsung');    // марка - samsung
$tv_samsung->setType('LSD TV');    // тип - жидкокристаллический
$tv_samsung->setDiagonal('127cm');    // диагональ - 127 сантиметров
echo $tv_samsung->getBrand();      // узнали марку
echo '<br />'; 
echo $tv_samsung->getType();    // узнали тип
echo '<br />'; 
echo $tv_samsung->getDiagonal();  // узнали диагональ
echo '<br />'; 
$tv_samsung->turnOn();   // нажали на кнопку и включили
echo '<br /><br />'; 

$parker = new Ballpen();    // новая ручка
$rayter = new Ballpen();    // новая ручка
$rayter->setBrand('rayter');    // марка новой ручки - rayter
$rayter->setType('gel');    // тип новой ручки - гелевая
$rayter->turnOn();      // нажал на кнопку на обратном концце ручки, видно стержень
$rayter->turnOff();      // нажал на кнопку, стержень не видно
echo $rayter->getBrand();   //  узнал марку ручки
echo '<br />'; 
echo $rayter->getType();    // узнал тип - гелевая
echo '<br />'; 
var_dump($rayter ->getRod());    // вынул стержень
echo '<br />'; 
var_dump($rayter ->insertRod());    // вставил стержень
echo '<br />'; 
echo $rayter ->write('Hello world!');    // написал слова
echo '<br /><br />'; 

$Krya = new Duck('Krya');      // новая утка, зовут Кря
$Donald = new Duck('Donald');    // новая утка, зовут Дональд
$Donald ->floats();          // Дональд плавает
$Donald ->walking();       // Дональд ходит
$Donald ->fly();          // Дональд летает
echo 'утка в воде? '; 
var_dump($Donald ->onTheAir); 
echo '<br />'; 
echo $Donald ->name;   // узнаем имя Дональда
echo '<br /><br />'; 

$philips = new Product('smartfon', 'Philips W6500');    // новый смартфон филлипс
$samsung = new Product('smartfon', 'Samsung Galaxy S7');    // новый смартфон самсунг
$samsung ->setPrice(35000);            // установили цену самсунга
$samsung ->discount = 7;              // установили скидку
echo $samsung ->category;      // узнали категорию самсунга
echo '<br />'; 
echo $samsung ->brand;    // узнали марку
echo '<br />'; 
echo $samsung ->getPrice() . ' руб.';   // определили цену самсунга с учетом скидки
echo '<br /><br />'; 